import styled from "styled-components";
import { Button } from "../../../components/Main/Section02/MainSecButtonCTA";

const MainSection = styled.div` 
  max-width: 1080px; 
  margin: 0 auto; 
  display: flex; 
  flex-direction: column; 
  align-items: center; 
  justify-content: center;
  text-transform: uppercase;
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: 900;
  text-align: center;
`;

const SubTitle = styled.h2`
  font-size: 2rem;
  font-weight: 900;
  text-align: center;
  margin-top: 1.5rem;
`;

const TitleLarge = styled.p`
  font-size: 4rem;
  font-weight: 900;
`;

const TitleSmall = styled.p`
  font-size: 1rem;
  font-weight: 400;
  color: #666666;
  margin: 3rem 0;
`;

const CTAButton = styled(Button)`
  margin: 0 0 4rem 0;
`;

function MainSectionTitleCTA() {
  return (
    <MainSection>
      <Title>Con nuestro plan amigo querrás invitar a toda tu agenda</Title>
      <SubTitle>Conseguí</SubTitle>
      <TitleLarge>15€</TitleLarge>
      <Title>en descuentos, por cada amigo que traigas.</Title>
      <TitleSmall>Suscribe a nuestros paquetes</TitleSmall>
      <CTAButton>INSCRIBIRSE</CTAButton>
    </MainSection>
  );
}

export default MainSectionTitleCTA;