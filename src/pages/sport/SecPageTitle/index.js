import styled from "styled-components";
import { Button } from "../../../components/Main/Section02/MainSecButtonCTA";

const SectionTitle = styled.div`
  max-width: 1080px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: 900;
  text-align: center;
`;

const SubTitle = styled.h2`
  font-size: 2rem;
  font-weight: 400;
  text-align: center;
  margin-top: 1.5rem;
`;

const Description = styled.p`
  font-size: 2rem;
  font-weight: normal;
  text-transform: none;
  margin-top: 1.5rem;
`;

const SmallText = styled.p`
  font-size: 1rem;
  font-weight: 400;
  color: #666666;
  margin: 3rem 0;
`;

const CallToActionButton = styled(Button)`
  margin: 0 0 4rem 0;
`;

function SectionTitleCallToAction() {
  return (
    <SectionTitle>
      <Title>¿Te gusta la Fórmula 1?</Title>
      <SubTitle>¿Quieres conocer a Fernando Alonso?</SubTitle>
      <Description>Bicampeón de Fórmula 1</Description>
      <SmallText>
        suscribe un paquete con nuestros y gana inmediatamente dos entradas para ver la Fórmula 1
      </SmallText>
      <CallToActionButton>INSCRIBIRSE</CallToActionButton>
    </SectionTitle>
  );
}

export default SectionTitleCallToAction;