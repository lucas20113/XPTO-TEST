import styled from "styled-components";
import MainSecLeft from "./MainSecLeft";
import MainSecRight from "./MainSecRight";
import { GlobalSection } from "../../stylesGlobal";
import { GlobalCentralizeDiv } from "../../stylesGlobal";

const GlobalSection01 = styled(GlobalSection)`
    flex-direction: row;
    background-color: #F5F5F5;
`

const GlobalCentralizeDiv01 = styled(GlobalCentralizeDiv)`
    width: 100%;
    margin: 0 auto;
`

function Section01(){
    return (
        <GlobalSection01>
            <GlobalCentralizeDiv01>
                <MainSecLeft />
                <MainSecRight />
            </GlobalCentralizeDiv01>
        </GlobalSection01>
    )
}

export default Section01