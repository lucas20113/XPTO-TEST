import styled from "styled-components";

const Div = styled.div`
    display: flex;
    flex-direction: column;
    aling-items: center;
    justify-content: center;
    margin: 0 3rem;
`

const Title = styled.h1`
    color: black;
    font-weight: 400;
    font-size: 1.5rem;
`
const SubTitle = styled.h2`
    color: black;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 2rem;
`

function MainSecLeft(){
    return (
        <Div>
            <Title>EMPRESA XPTO</Title>
            <SubTitle>La tienda que te da premios</SubTitle>
        </Div>
    )
}

export default MainSecLeft