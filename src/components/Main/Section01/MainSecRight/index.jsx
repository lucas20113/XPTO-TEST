import styled from "styled-components";
import Logo from "../../../../imgs/Logo/Logo.png"

const SecImg = styled.img`
    width: 350px;
    height: 350px;
    margin: 0 3rem;
`

function MainSecRight() {
    return (
        <SecImg src={Logo} alt="logo"/>
     )
}

export default MainSecRight