import styled from "styled-components";
import { Carousel } from "react-bootstrap";
import "../../Carousel.css"
import imgs from '../MainSecImgs'


const StyledCarousel = styled(Carousel)`
    .carousel-control-prev, .carousel-control-next {
        display: none;
    }

    .carousel-indicators {
        display: flex;
        justify-content: center;
        align-items: center;
        bottom: -55px;
    }

    .carousel-indicators button {
        background-color: #000;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        margin-right: 10px;
        margin-left: 10px;
      }
      
      .carousel-indicators .active {
        background-color: #000;
      }

`;

function CarouselImgs() {
    return (
        <>
            <StyledCarousel interval={null}>
                {imgs.map((img, index) => (
                    <Carousel.Item key={index}>
                        <a href="sport">
                            <img
                                src={img}
                                alt='imgs'
                            ></img>
                        </a>
                    </Carousel.Item>
                ))}
            </StyledCarousel>
        </>
    )
}

export default CarouselImgs;


