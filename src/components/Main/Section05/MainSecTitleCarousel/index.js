import styled from "styled-components";

const Div = styled.div`
    margin: 2rem 0;
`

const Title = styled.h1`
    font-size: 2rem;
    font-weight: normal;
    text-align: center;
    margin: 2.5rem 0 1.5rem 0;
`

function MainSecTitleCarousel() {
    return (
        <Div>
            <Title>EMPRESA XPTO <strong>CON EL DESPORTE ESPAÑOL</strong></Title>
        </Div>
    )
}

export default MainSecTitleCarousel