import styled, { keyframes } from "styled-components";
import icon1 from "../iconService/icon1.png"
import icon2 from "../iconService/icon2.png"
import icon3 from "../iconService/icon3.png"
import icon4 from "../iconService/icon4.png"

const Icons = [icon1, icon2, icon3, icon4]
const SubTitlesIcons = [
    "Los mejores servicios al mejor precio", 
    "La mejor cobertura de Vodafone", 
    "Acumula GB para el mes siguiente", 
    "Comparte GB con tus amigos Fi"
]

const Div = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    gap: 1.5rem;
`

const bounce = keyframes`
    from, 20%, 53%, 80%, to {
        transform: translate3d(0,0,0);
    }

    40%, 43% {
        transform: translate3d(0, -20px, 0);
    }

    70% {
        transform: translate3d(0, -10px, 0);
    }

    90% {
        transform: translate3d(0,-5px,0);
    }
`;

const DivImg = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    width: 350px;
    height: 250px;

    &:hover {
        animation: ${bounce} 0.8s cubic-bezier(0.175, 0.885, 0.32, 1.275) both;
    }
`

const P = styled.p`
    font-size: 1.5rem;
    font-weight: normal;
    text-align: center;
    text-transform: uppercase;
    margin-top: 1rem;
`

function MainSecIconsService() {
    return <Div>
        
        {Icons.map((n, i) => 
            <DivImg>
                <img src={n} alt={n}/>
                <P>{SubTitlesIcons[i]}</P>
            </DivImg>)
        }
        </Div>
}

export default MainSecIconsService