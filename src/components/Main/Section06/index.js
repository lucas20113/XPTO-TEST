import styled from "styled-components";
import MainSecIconsService from "./MainSecIconsService"
import { GlobalSection } from "../../stylesGlobal";

const GlobalSection6 = styled(GlobalSection)`
    flex-direction: row;
`

const Div = styled.div`
    max-width: 1080px;
    margin: 0 auto;
`

const Title = styled.h1`
    font-size: 2rem;
    font-weight: 900;
    text-align: center;
    margin: 4.5rem 0 1rem;
`

function Section06() {
    return (
        <GlobalSection6>
            <Div>
                <Title>¿Qué ofrecemos?</Title>
                <MainSecIconsService />
            </Div>
        </GlobalSection6>
    )
};

export default Section06;