import styled from "styled-components";

const Div = styled.div`
    text-align: center;
    max-width: 1080px;
`

const Title = styled.h1`
    font-size: 2rem;
    font-weight: 900;
    text-align: center;
    text-transform: uppercase;
    margin: 4rem 0 2rem 0;
`
const SubTitle = styled.h2`
    text-transform: uppercase;
    font-weight: 400;
    margin: 0.5rem 0rem;
    font-size: 1.5rem;
    margin: 0 0 3rem 0;
    line-height: 1.5;
`

function MainSecFriend() {
    return (
        <Div>
            <Title>
            nuevo plan amigo
            </Title>
            <SubTitle>
            Invita a un amigo y gana 15€ para ti y para él. Trae a tantos como quieras, ¡Es ilimitado!
            </SubTitle>
        </Div>
    )
}

export default MainSecFriend