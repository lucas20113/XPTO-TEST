import styled from "styled-components";
import MainSecFriend from "./MainSecFriend";
import MainSecFriendImg from "./MainSecFriendImg";
import { Button } from "../Section02/MainSecButtonCTA";
import { GlobalSection } from "../../stylesGlobal";

const GlobalSection4 = styled(GlobalSection)`
    position: relative;
`

const NewButton = styled(Button)`
    position: absolute;
    top: 97%;
    left: 50%;
    transform: translate(-50%, -50%);
    cursor: pointer;
`;

function Section04() {
    return (
        <GlobalSection4>
            <MainSecFriend />
            <MainSecFriendImg />
            <a href="friend">
                <NewButton>Descubre el plan amigo</NewButton>
            </a>
        </GlobalSection4>
    )
};

export default Section04;