import styled from "styled-components";

const Div = styled.div`
    text-align: center;
    width: 1000px;
`

const Title = styled.h1`
    font-size: 2rem;
    font-weight: 900;
    text-align: center;
    text-transform: uppercase;
    margin: 2.5rem 0 1.5rem 0;
`
const SubTitle = styled.h2`
    text-transform: uppercase;
    font-weight: 400;
    margin-bottom: 1.5rem;
    font-size: 1.5rem;

`

function MainSecApp() {
    return (
        <Div>
            <Title>
                ¡Tenemos una app!
            </Title>
            <SubTitle>
                ¡Descárgalo y analiza tu consumo ahora!
            </SubTitle>
        </Div>
    )
}

export default MainSecApp