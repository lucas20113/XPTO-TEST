import styled from "styled-components";

const Div = styled.div`
    text-align: center;
    width: 1000px;
`

const Button = styled.button`
    position: absolute;
    top: 97%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 300px;
    height: 65px;
    border: none;
    background-color: #000;
    color: #f5f5f5;
    font-size: 1.5rem;
    font-weight: normal;
    border-radius: 5px;
    margin: 1.5rem 0rem;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 3px 5px 7px rgba(0, 0, 0, 0.25);
    cursor: pointer;

    &:hover {
        background-color: #BFBFBF;
        i, p { color: black };
    }

    i {
        font-size: 33px;
        text-align: left;
        margin: 0 0.5rem
    }

    p {
        color: #fff;
        margin-bottom: 0;
    }
`

function MainSecAppButton() {
    return (
        <Div>
            <Button>
                <i className='fab fa-google-play'></i>
                <p>Google Play</p>
            </Button>
        </Div>
    )
}

export default MainSecAppButton