import styled from "styled-components";
import { useState } from 'react';
import PopDesign from '../../../PopUp';

const Div = styled.div`
    text-align: center;
    width: 1000px;
`

const Title = styled.h1`
    font-size: 2rem;
    font-weight: 900;
    text-align: center;
    text-transform: uppercase;
    margin: 2.5rem;
    line-height: 1.5;
`

const SubTitle = styled.h2`
    text-transform: uppercase;
    font-weight: 400;
    margin: 0.5rem 0rem;
    font-size: 1.5rem;
    line-height: 1.5;
`

const Button = styled.button`
    width: 300px;
    height: 65px;
    border: none;
    background-color: #000;
    color: #f5f5f5;
    font-size: 1.5rem;
    font-weight: normal;
    border-radius: 5px;
    margin: 1.5rem 0rem;
    box-shadow: 3px 5px 7px rgba(0, 0, 0, 0.25);
    cursor: pointer;


    &:hover {
        background-color: #BFBFBF;
        color: black;        
      }
`

function MainSecButtonCTA() {

    const [open, setOpen] = useState(false);

    const popUpOpen = () => {
        setOpen(true);
    }

    const popUpClose = () => {
        setOpen(false);
    }

    return (
        <Div>
            <Title>
                ¿Quieres saber si tenemos cobertura de fibra en tu hogar?
            </Title>
            <SubTitle>
                Míralo aquí y contrata tu tarifa de fibra +móvil o sólo fibra de forma sencilla
            </SubTitle>
            <Button onClick={popUpOpen}>
                Comprobar cobertura
            </Button>
            {open && <PopDesign onClose={popUpClose}/>}
        </Div>
    )
}

export { MainSecButtonCTA, Button }