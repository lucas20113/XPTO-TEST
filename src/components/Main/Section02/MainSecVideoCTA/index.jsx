import styled from "styled-components";
import { Carousel } from "react-bootstrap";
import "../../Carousel.css"

const Videos = [
    "https://www.youtube-nocookie.com/embed/thSPGPnuuNg",
    "https://www.youtube-nocookie.com/embed/28xeZIh_cqA",
    "https://www.youtube-nocookie.com/embed/C4-Qg7Owurc",
];

const Iframe = styled.iframe`
    margin-bottom: 2rem;
`

const StyledCarousel = styled(Carousel)`
    .carousel-control-prev, .carousel-control-next {
        display: none;
    }

    .carousel-indicators {
        display: flex;
        justify-content: center;
        align-items: center;
        bottom: -25px;
    }

    .carousel-indicators button {
        background-color: #000;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        margin-right: 10px;
        margin-left: 10px;
      }
      
      .carousel-indicators .active {
        background-color: #000;
      }

`;

function MainSecVideoCTA() {
    return (
    <StyledCarousel interval={null} variant="dark">
        {Videos.map((video, index) => (
            <Carousel.Item key={index}>
                <Iframe
                    width="854"
                    height="480"
                    src={video}
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen
                ></Iframe>
            </Carousel.Item>
        ))}
    </StyledCarousel>
    )
}

export default MainSecVideoCTA