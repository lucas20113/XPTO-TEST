import styled from "styled-components";

const Title = styled.h1`
    font-size: 2rem;
    font-weight: 900;
    text-align: center;
    text-transform: uppercase;
    margin: 2.5rem;
`

function MainSecTitleCTA(){
    return <Title>¿A qué esperas? ¡Elige tu regalo!</Title>
}

export default MainSecTitleCTA