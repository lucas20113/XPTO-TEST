import styled from "styled-components";
import { GlobalCentralizeDiv } from "../../stylesGlobal";

const textOption = [
  { id: 1, name: 'Plan Amigo', link: 'friend' },
  { id: 2, name: 'Regalo', link: 'sport' },
  { id: 3, name: 'App', link: '/' },
  { id: 4, name: 'Preguntas', link: 'faq' },
];

const OptionsList = styled.li`
  padding: 0 2rem;
  min-width: 7.5rem;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  list-style: none;

  a {
    text-decoration: none;
    color: #000;
    text-align: center;
    font-size: 1rem;
    font-weight: 400;
    
    &:hover {
      color: #fff;
      cursor: pointer;
    }
  }
`

function OptionsHeader() {

  const pageVerification = window.location.pathname === "/";

  return (
    <GlobalCentralizeDiv>
      {textOption.map((text, key) => <OptionsList key={key}><a href={pageVerification && text.link === "/" ? "#app" : text.link}>{text.name}</a></OptionsList>)}
    </GlobalCentralizeDiv>
  )
};

export default OptionsHeader;