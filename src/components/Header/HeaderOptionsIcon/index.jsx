import style from "styled-components";
import { GlobalCentralizeDiv } from "../../stylesGlobal";

const person = "person";

const IconList = style.div`
    align-items: center;
    cursor: pointer;
    display: flex;
    justify-content: center;

    span {
        &:hover {
            color: #fff;
            cursor: pointer;
        }
    }
`;

function OptionsIconHeader() {
    return (
       <>
            <GlobalCentralizeDiv>
                <IconList>
                    <span className="material-icons">{person}</span>
                </IconList>
            </GlobalCentralizeDiv>
        </>
    )
};

export default OptionsIconHeader;

