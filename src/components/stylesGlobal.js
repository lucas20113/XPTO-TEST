import styled from 'styled-components';

export const GlobalCentralizeDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const GlobalSection = styled.section`
    width: 100%;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`