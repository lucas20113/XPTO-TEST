import styled from "styled-components";

const A = styled.a`
    display: flex;
    flex-direction: column;
    margin: 0.5rem 0;
    text-align: left;
    cursor: pointer;
    &:hover {
        color: #fff
    }
`

function Links(props) {
    return (
        <>
            {props.text === props.tarifas && props.subtarifas.map((t, k) => <A>{t}</A>)}
        </>)

}

export default Links;