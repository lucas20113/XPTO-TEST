import styled from "styled-components";

const LinksPoliticas = ['Aviso legal', 'Política de cookies', 'Política de privacidad', 'Términos y condiciones']

const Div = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`

const Icons = styled.i`
    font-size: 2.5rem;
    margin: 1rem 1rem;
    cursor: pointer;
    &:hover {
        color: #fff
    }
`

const Copy = styled.p`
    font-size: 0.8rem;
    margin-top: 1rem;
`
const A = styled.a`
    margin: 0 1rem;
    cursor: pointer;
    &:hover {
        color: #fff
    }
`

function PoliticasIcons(){
    return (
    <>
        <Div>
            <Icons className="fa-brands fa-square-facebook"></Icons>
            <Icons className="fa-brands fa-youtube"></Icons>
            <Icons className="fa-brands fa-linkedin"></Icons>
            <Icons className="fa-brands fa-twitter"></Icons>
        </Div>

        <Div>{LinksPoliticas.map(text => <A>{text}</A>)}</Div>

        <Div><Copy>@EMPRESAXPTO - All Rights Reserved - 2023</Copy></Div>
    </>

    )
}

export default PoliticasIcons;